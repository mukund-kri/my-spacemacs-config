
;; Manually setting up windmove with control-arrow keys.
;; I did get it to work with (windmove-default-keybinding 'meta) but org-mode would
;; not give up its Alt bindings. I'm happy with these binding for now. 
(global-set-key (kbd "C-<left>")  'windmove-left)
(global-set-key (kbd "C-<right>") 'windmove-right)
(global-set-key (kbd "C-<up>")    'windmove-up)
(global-set-key (kbd "C-<down>")  'windmove-down)

;; In addition to tab also bind Ctrl-Tab to yas-expand so that there is an
;; alternative in org mode 
(define-key yas-minor-mode-map (kbd "C-<tab>") 'yas-expand)


;; Set F8 to toggle neo tree as recommended in neo tree docs
(global-set-key [f8] 'neotree-toggle)
