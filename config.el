;;;; LOOK 'n FEEL
;; Theme
(spacemacs/load-theme 'gruvbox)
(load-theme 'gruvbox-dark-soft t)




;;;; Configuration
(setq-default

 ;;;; ORG-MODE CONFIG
 ;; My TODO work flow is better suited with a couple of extra states
 org-todo-keywords '((sequence "TODO" "IN_PROGRESS" "|" "DONE" "ABANDONDED"))

 ;;;; JAVASCIRPT SYNTAX CONFIG
 js2-basic-offset 2
 js-indent-level 2
 css-indent-offset 2
 web-mode-markup-indent-offset 2
 web-mode-css-indent-offset 2
 web-mode-code-indent-offset 2
 web-mode-attr-indent-offset 2

 ;; Tell coffee mode where the coffee linting configuration is
 flycheck-coffeelintrc "coffeelint.json"

 auto-completion-private-snippets-directory
 '("~/.emacs.d/private/snippets.d/react-snippets/"
   "~/.emacs.d/private/snippets.d/org-snippets/"
   "~/.emacs.d/private/snippets.d/java-snippets/"
   "~/.emacs.d/private/snippets.d/scala-snippets/"
   "~/.emacs.d/private/snippets.d/backbone-marionette-snippets/")

 ;; prevent the emacs modes from adding their own indents in the snippets
 yas-indent-line nil

 ;;;; MAGIT
 ;; Dirs which hold most of code. For use with magit-list-repository
 magit-repository-directories
 '(
   ;; My repos on gitlab mostly my teaching material
   ("~/projects/" . 2)

   ;; Dot files
   ("~/dotfiles/". 1)

   ;; Orgs
   ("~/orgs/" . 1)

   ;; My cookiecutter templates
   ("~/my-cookiecutters" . 1)

   ;; Regalix work
   ("~/regalix/" . 2)

   ;; My emacs / spacemacs config
   ("~/.emacs.d/private" . 3)
   )

 ;; Magit-list-repository list format
 magit-repolist-columns
 '(("Name"    25 magit-repolist-column-ident                  ())
   ("Version" 25 magit-repolist-column-version                ())
   ("D"        1 magit-repolist-column-dirty                  ())
   ("L<U"      3 magit-repolist-column-unpulled-from-upstream ((:right-align t)))
   ("L>U"      3 magit-repolist-column-unpushed-to-upstream   ((:right-align t)))
   ("Path"    99 magit-repolist-column-path                   ()))
 )


;; More web-mode config
(with-eval-after-load 'web-mode
  (add-to-list 'web-mode-indentation-params '("lineup-args" . nil))
  (add-to-list 'web-mode-indentation-params '("lineup-concats" . nil))
  (add-to-list 'web-mode-indentation-params '("lineup-calls" . nil)))

;;;; GTD experiments
(setq org-agenda-files '("~/orgs/orgs/gtd/inbox.org"
                         "~/orgs/orgs/gtd/gtd.org"
                         "~/orgs/orgs/gtd/tickler.org"))

(setq org-capture-templates
      '(
        ("t" "Todo[INBOX]" entry
         (file+headline "~/orgs/orgs/gtd/inbox.org" "Tasks")
         (file "~/orgs/orgs/templates/todotemplate.txt")
         :empty-lines 1
         )
        ("T" "Tickler" entry
         (file+headline "~/orgs/orgs/gtd/tickler.org" "Tickler")
         "* %i%? \n %U")
        ("q" "Open Question[NOTES]" entry
         (file+headline "~/orgs/orgs/notes/openquestion.org" "Open Questions")
         (file "~/orgs/orgs/templates/openquestion.txt")
         :empty-lines 1
         )
      ))

(setq org-refile-targets '(("~/orgs/orgs/gtd/gtd.org" :maxlevel . 2)
                           ("~/orgs/orgs/gtd/someday.org" :level . 1)
                           ("~/orgs/orgs/gtd/on-hold.org" :level . 1)
                           ("~/orgs/orgs/gtd/tickler.org" :maxlevel . 2)
                           ))

(add-to-list 'exec-path "~/.virtualenvs/django/bin/")
;; (pythonic-activate "~/.virtualenvs/django")


